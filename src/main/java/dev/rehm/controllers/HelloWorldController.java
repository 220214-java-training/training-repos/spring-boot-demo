package dev.rehm.controllers;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
//@CrossOrigin
public class HelloWorldController {

//    @RequestMapping(value = "/hello", method = RequestMethod.GET, produces = "text/plain")
//    @ResponseBody
//    public ResponseEntity<String> displayMessage(HttpServletRequest request,
//                                                @RequestParam(value = "name",required = false) String name){
//        if(name==null){
//            name = "Guest";
//        }
//        return new ResponseEntity<>("Hello World "+name+"! "+request.getMethod()
//                +" request made to: "+request.getRequestURI(), HttpStatus.OK);
//    }

    @GetMapping("/hello")
    @ResponseBody
    public String sayHi(){
        return "Hello World";
    }

}
